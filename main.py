from fastapi import FastAPI
from fastapi.responses import HTMLResponse

from config.database import engine, Base

from middlewares.error_handlers import ErrorHandler

from routers.movie import movie_router
from routers.user import user_router



#FastAPI
app = FastAPI()
app.title = "Mi aplicación con FastAPI"
app.version = "0.0.1"

#Middleware
app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

#SQLAlchemy
Base.metadata.create_all(bind=engine)

# movies = [
#     {
#         "id": 1,
#         "title": "Avatar",
#         "overview": "En un exuberante planeta llamdado Pantera pasan consas increibles",
#         "year": 2009,
#         "rating": 7.8,
#         "category": "Acción"
#     },
#     {
#         "id": 2,
#         "title": "Terminator",
#         "overview": "El fin del mundo",
#         "year": 2010,
#         "rating": 8.9,
#         "category": "Acción"
#     }
# ]


@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hello World</h1>')



from pydantic import BaseModel, Field
from typing import Optional, List


class Movie(BaseModel):
    id : Optional[int]= None
    title : str = Field(min_length=2,max_length=30)
    overview : str = Field(min_length=2,max_length=30)
    year : int = Field(le=2022)
    rating : float = Field(gt=1,le=10)
    category : str = Field(min_length=1, max_length=10)

    class Config:
        schema_extra= {
            "example" : {
                "id": 1,
                "title": "Mi pelicula",
                "overview": "La mejor pelicula",
                "year": 2022,
                "rating": 9.8,
                "category": "Comedia"
            }
        }
from fastapi import APIRouter, Path, Query, Depends, status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from schemas.movie import Movie

from typing import List

from config.database import Session

from middlewares.jwt_bearer import JWTBearer

from services.movie import MovieService

movie_router = APIRouter()



#get method
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], dependencies=[Depends(JWTBearer())])
def get_movies():
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(content=jsonable_encoder(result))

#path parameter
@movie_router.get('/movies/{id}', tags=['movies'])
def get_movie(id: int = Path(ge=1, le=2000)):
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"message": "Not found by id"})
    
    # for item in movies:
    #     if item["id"] == id:
    #         return JSONResponse(content=item)
    return JSONResponse(status_code=status.HTTP_200_OK, content=jsonable_encoder(result))


#query parameters
@movie_router.get('/movies/', tags=['movies'])
def get_movies_by_category(category : str = Query(min_length=5, max_length=15)):
    db = Session()
    result = MovieService(db).get_movies_with_category(category)
    if not result:
        return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"message": "Not found by category"})
    # category_movie = [item for item in movies if item['category'] == category]
    return JSONResponse(status_code=status.HTTP_200_OK, content=jsonable_encoder(result))

#Post parameters
@movie_router.post(
    '/movies', 
    tags=['movies']
    )
def create_movie(movie : Movie):
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(content={"message": "Movie succesfully created"})

#Put method
@movie_router.put(
    '/movies',
    tags=['movies']
    )
def update_movie(id: int, movie : Movie):
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"message": "Not found by id"})
    
    MovieService(db).update_movie(id,movie)
    return JSONResponse(content={"message": "Se ha modificado la pelicula"})

@movie_router.delete(
    '/movies',
    tags=['movies']
    )
def delete_movie(id: int):
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"message": "Not found by id"})
    MovieService(db).delete_movie(id)

    return JSONResponse(content={"message": "Se ha eliminado la pelicula"})
    # for item in movies:
    #     if item['id'] == id:
    #         movies.remove(item)
    #         return movies